/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package superkey.keychain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author hugofernandes
 */
public class KeyEntryTest {
    private KeyEntry entryA;
    
    public KeyEntryTest() {
    }
    
    @Before
    public void setUp() {
        entryA = new KeyEntry();
        entryA.setApplicationName("appx");
        entryA.setUsername("xx");
        entryA.setPassword("secret@@@");
        
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testGetPassword()
    {
        assertEquals("failed to get password", "secret@@@", entryA.getPassword());
    }
    
    @Test( expected = IllegalArgumentException.class)
    public void testSetPasswordWithNull() {
        entryA.setPassword(null);
    }
    
    @Test( expected = IllegalArgumentException.class)
    public void testSetPasswordWithEmpty() {
        entryA.setPassword("");
    }

    @Test
    public void testGetUsername()
    {
        assertEquals("failed to get username", "xx", entryA.getUsername());
    }
    
    @Test( expected = IllegalArgumentException.class)
    public void testSetUsernameWithNull() {
        entryA.setUsername(null);
    }
    
    @Test( expected = IllegalArgumentException.class)
    public void testSetUsernameWithEmpty() {
        entryA.setUsername("");
    }

    @Test
    public void testGetApplicationName()
    {
        assertEquals("failed to get username", "appx", entryA.getApplicationName());
    }
            
    @Test( expected = IllegalArgumentException.class)
    public void testSetApplicationNameWithNull() {
        entryA.setApplicationName( null);
    }
    
    @Test( expected = IllegalArgumentException.class)
    public void testSetApplicationNameWithEmpty() {
        entryA.setApplicationName("");
    }

    @Test
    public void testKey() {
        // the key is the application name
        assertEquals("failed to get existing key field", entryA.key(), "appx");
    }

    @Test
    public void testFormatAsCsv() {
        String expects = "appx" + KeyEntry.FIELDS_DELIMITER + "xx" + KeyEntry.FIELDS_DELIMITER + "secret@@@";
        assertEquals("failed to format entry to delimited string", entryA.formatAsCsv(), expects);                  
    }

    @Test
    public void testToString() {
        String expects = entryA.getApplicationName() + "\t" + entryA.getUsername() + "\t" + entryA.getPassword();
        assertEquals("failed to string", entryA.toString(), expects);
    }

    @Test
    public void testParse() {
        String csvLine = "appx" + KeyEntry.FIELDS_DELIMITER + "xx" + KeyEntry.FIELDS_DELIMITER + "secret@@@";
        KeyEntry retValue = entryA.parse(csvLine);
        assertEquals("failed to parsing application name", retValue.getApplicationName(), "appx");
        assertEquals("failed to parsing username", retValue.getUsername(), "xx");
        assertEquals("failed to parsing password", retValue.getPassword(), "secret@@@");
    }
    
}
