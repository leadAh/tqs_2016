/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package superkey.keychain;

import java.io.File;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchProviderException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author hugofernandes
 */
public class CipherToolTest {
    
    public CipherToolTest() {
    }
    
    private CipherTool cipher;
    private KeyChain kchainA;
    
    @Before
    public void setUp() throws IOException {
        cipher = new CipherTool("masterkey");
        kchainA = new KeyChain(new File("test.txt"), cipher);
        cipher.writeProtectedKeychain(kchainA.toString(), new File("test.txt"));
        
    }
    
    @After
    public void tearDown() {
    }
    
    @Test (expected = IOException.class)
    public void testReadProtectedKeychainIfFails() throws IOException
    {
        cipher.readProtectedKeychain(kchainA, new File("test2.txt"));
    }

    
    @Test
    public void testFail() 
    {
        fail();
    }
}
