/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package superkey.keychain;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author hugofernandes
 */
public class KeyChainTest {
    private KeyEntry entryA, entryB;
    private KeyChain chainA, emptyChain;
    private CipherTool cipherA;
    
    public KeyChainTest() {
    }
    
    @Before
    public void setUp() {
        entryA = new KeyEntry();
        entryA.setApplicationName("a");
        entryA.setUsername("xx");
        entryA.setPassword("secret@@@");
        
        entryB = new KeyEntry();
        entryB.setApplicationName("b");
        entryB.setUsername("yy");
        entryB.setPassword("secret***");
        
        cipherA = new CipherTool("masterkey");
        
        chainA = new KeyChain(new File("file.txt"), cipherA);
        chainA.put(entryB);
        chainA.put(entryA);
        
        emptyChain = new KeyChain(new File("emptyFile.txt"), cipherA);        
    }
    
    @After
    public void tearDown() {
    }
    
    @Test( expected = IllegalArgumentException.class)
    public void testPutExistingEntry()
    {
        chainA.put(entryA);
    }
    
    @Test
    public void testPutWithValidValue()
    {
        KeyEntry entryC = new KeyEntry();
        entryC.setApplicationName("app1");
        entryC.setUsername("username");
        entryC.setPassword("psw");
        
        chainA.put(entryC);
        assertEquals("failed to put value", entryC, chainA.find("app1"));
    }
    
    @Test
    public void testFindNull()
    {
        assertEquals("failed to find value", null, chainA.find("naoexiste"));
    }
    
    @Test
    public void testFindExistingKey()
    {
        assertEquals("failed to find key", entryA, chainA.find(entryA.getApplicationName()));
    }
    
    @Test (expected = IOException.class)
    public void testFromWithInvalidData() throws IOException
    {
        chainA.from(new File("file.txt"), new CipherTool("invalidkey"));
    }

    public void testFromWithValidData() throws IOException
    {
        assertEquals("failed to form", chainA.from(new File("file.txt"), new CipherTool("masterkey")), chainA);
    }
    
    @Test
    public void testAllEntries()
    {
        Iterable<KeyEntry> allEntries = chainA.allEntries();
        Iterator iterator = allEntries.iterator();
        boolean entryAinAllEntries = false;
        boolean entryBinAllEntries = false;
        
        while(iterator.hasNext())
        {
            KeyEntry ke = (KeyEntry) iterator.next();
            
            if(ke == entryA)
                entryAinAllEntries = true;
            
            if(ke == entryB)
                entryBinAllEntries = true;           
        }
        
        assertEquals("failed to show all entries", true, entryAinAllEntries);
        assertEquals("failed to show all entries", true, entryBinAllEntries);
    }
    
    @Test
    public void testAllEntriesWithNoEntries()
    {
        
        assertEquals("failed to show all AllEntriesWithNoEntries", false, emptyChain.allEntries().iterator().hasNext());
    }

    @Test
    public void testSortedEntries()
    {
       
       Iterable<KeyEntry> allEntries = chainA.sortedEntries();
       Iterator iterator = allEntries.iterator();
       int posicao = 0;
               
       while(iterator.hasNext())
       {
           KeyEntry ke = (KeyEntry) iterator.next();

           if(posicao == 0)
               assertEquals("failed to sort entries", "a", ke.getApplicationName());
           else
               assertEquals("failed to sort entries", "b", ke.getApplicationName());
           
           posicao++;
       }
     
    }

    @Test
    public void testToString()
    {
       chainA.sortedEntries();
       String expected = entryA.toString() + "\n" + entryB.toString() + "\n"; 
       
       assertEquals("failed to format as csv", expected, chainA.toString());
        
    }

}